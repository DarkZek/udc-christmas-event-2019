CREATE USER 'challenge1'@'%' IDENTIFIED BY 'challenge1';
GRANT SELECT ON udc.challenge1 TO 'challenge1'@'%';
FLUSH PRIVILEGES;

create table challenge1 (
    id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    username VARCHAR(30) NOT NULL,
    password VARCHAR(30) NOT NULL);

INSERT INTO challenge1 (username, password) VALUES ("admin", "unPwnable_5543");