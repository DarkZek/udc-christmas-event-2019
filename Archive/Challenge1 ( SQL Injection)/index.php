<html>
<head>
    <title>FIB - Sign In</title>
    <link href="/css/app.css" rel="stylesheet">
    <link href="./css/app.css" rel="stylesheet">
    <link rel="shortcut icon" href="./favicon.png">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script>
        window.onload = function() {
            $("form").on('submit', function(event) {

                var username = $("*[name=username]")[0].value;
                var password = $("*[name=password]")[0].value;

                $.get("./login.php?username=" + username + "&password=" + password, null, function(result) {
                    alert(result);
                });
                return false;
            });
        };
    </script>
</head>

<body>
    <!-- TODO: Remove login page backups @ ./login.backup -->
    <div class="card shadow">
        <div class="col title">
            <img src="./favicon.png">
            <h1><a>F</a>ederal</h1>
            <h1><a>I</a>nvestigation</h1>
            <h1><a>B</a>uraeu</h1>
        </div>
        <form class="col login">        
            <input name="username" class="form-control" placeholder="Username">
            <br>
            <input name="password" class="form-control" placeholder="Password" type="password">
            <br>
            <input class="form-control" type="submit" value="LOGIN">
        </form>
    </div>
    <a href="https://gitlab.com/darkzek/" target="_blank">Secured by Zek Systems International</a>
</body>
</html>