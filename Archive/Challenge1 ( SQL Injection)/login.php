<?php

if (!isset($_GET["username"]) || !isset($_GET["password"])) {
    die("Username or password not set");
}

//Fetch variables
$username = urldecode($_GET["username"]);
$password = urldecode($_GET["password"]);

//Connect to database
$host = '172.18.0.2';
$db   = 'udc';
$user = 'challenge1';
$pass = 'challenge1';
$charset = 'utf8mb4';

$dsn = "mysql:host=$host;dbname=$db;charset=$charset";
$options = [
    PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
    PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
    PDO::ATTR_EMULATE_PREPARES   => false,
];
try {
     $pdo = new PDO($dsn, $user, $pass, $options);
} catch (\PDOException $e) {
     throw new \PDOException($e->getMessage(), (int)$e->getCode());
}

$query = 'SELECT * FROM challenge1 WHERE password = "' . $password . '" AND username = "' . $username . '";';

$stmt = $pdo->query($query);
if ($stmt->fetch() != null) {
    die("Welcome, agent! The key is 'unPwnable_5543'");
} else {
    die("Incorrect username/password");
}