<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/TOP_SECRET_NUCLEAR_LAUNCH_CODES/run', function () {
    ob_start();
    include('/home/darkzek/projects/udc-christmas-event-2019/Challenge 2/run.php');
    $returned = ob_get_contents();
    ob_end_clean();

    return $returned;
})->middleware("throttle:2:1");

Route::get('/TOP_SECRET_NUCLEAR_LAUNCH_CODES/code', function () {
    ob_start();
    include('/home/darkzek/projects/udc-christmas-event-2019/Challenge 2/code.php');
    $returned = ob_get_contents();
    ob_end_clean();

    return $returned;
})->middleware("throttle:6:1");
