<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://fonts.googleapis.com/css?family=Raleway:900&display=swap" rel="stylesheet"> 

        <title>UDC Christmas Event</title>
        <meta property="og:title" content="UDC Christmas Event">
        <meta property="og:description" content="UDC Christmas Event is a yearly event held for the UDC discord group to celebrate the year with some fun. Christmas is fast approaching, the Grinch is up to something. What? Thats your job. Connect to remote servers, solve authentication methods and complete the challenge.">
        <meta property="og:image" content="https://christmas.unitydevcommunity.com/challenge5/image.gif" />

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">
        <link href="/css/app.css" rel="stylesheet">
    </head>
    <body>
        <div class="challenges">
            <div class="title">
                <div class="container">
                    <a>UDC Christmas Event</a>
                </div>
            </div>
            <object id="breaker1" data="/storage/breaker1.svg" type="image/svg+xml"></object>
            <div class="schedule">
                <div class="container">
                    <h1>The Grinch That Stole Christmas</h1>
                    <br>
                    <p>UDC Christmas Event is a yearly event held for the UDC discord group to celebrate the year with some fun. Christmas is fast approaching, the <b>Grinch</b> is up to something. What? Thats your job. Connect to remote servers, solve authentication methods and complete the challenge.<br></p>     
                    <br>
                    <div class="row">
                        <div class="col-md-1"></div>
                        <div class="col-md-2">
                            <div class="card">
                                <a>Challenge 1</a>
                                <br>
                                <a>The grinch that stole christmas</a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card">
                                <a>Challenge 2</a>
                                <br>
                                <a>The password replacement</a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card">
                                <a>Challenge 3</a>
                                <br>
                                <a>Why you dont make custom auth solutions</a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card">
                                <a>Challenge 4</a>
                                <br>
                                <a>Blow it sky high</a>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="card">
                                <a>Challenge 5</a>
                                <br>
                                <a>A cautionary tale</a>
                            </div>
                        </div>
                        <div class="col-md-1"></div>
                    </div>
                    <br>
                    <a class="desktop-only">After all these challenges are completed santa's got a reward for you!</a>
                    <br>
                    <br>
                    <div class="row desktop-only">
                        <div class="col-4">
                            <svg style="width:150px;height:150px" viewBox="0 0 24 24">
                                <path fill="#ffb700" d="M12,7L17,12H14V16H10V12H7L12,7M21,16.5C21,16.88 20.79,17.21 20.47,17.38L12.57,21.82C12.41,21.94 12.21,22 12,22C11.79,22 11.59,21.94 11.43,21.82L3.53,17.38C3.21,17.21 3,16.88 3,16.5V7.5C3,7.12 3.21,6.79 3.53,6.62L11.43,2.18C11.59,2.06 11.79,2 12,2C12.21,2 12.41,2.06 12.57,2.18L20.47,6.62C20.79,6.79 21,7.12 21,7.5V16.5M12,4.15L5,8.09V15.91L12,19.85L19,15.91V8.09L12,4.15Z" />
                            </svg>
                            <br>
                            <a>5,000 XP added to your UDC account</a>
                        </div>
                        <div class="col-4">
                            <svg style="width:150px;height:150px" viewBox="0 0 24 24">
                                <path fill="#ffb700" d="M21 11C21 16.55 17.16 21.74 12 23C6.84 21.74 3 16.55 3 11V5L12 1L21 5V11M12 21C15.75 20 19 15.54 19 11.22V6.3L12 3.18L5 6.3V11.22C5 15.54 8.25 20 12 21M15.05 16L11.97 14.15L8.9 16L9.71 12.5L7 10.16L10.58 9.85L11.97 6.55L13.37 9.84L16.95 10.15L14.23 12.5L15.05 16" />
                            </svg>
                            <br>
                            <a>A unique rank on your UDC profile</a>
                        </div>
                        <div class="col-4">
                            <svg style="width:150px;height:150px" viewBox="0 0 24 24">
                                <path fill="#ffb700" d="M17,3H14V6H10V3H7A2,2 0 0,0 5,5V21A2,2 0 0,0 7,23H17A2,2 0 0,0 19,21V5A2,2 0 0,0 17,3M12,8A2,2 0 0,1 14,10A2,2 0 0,1 12,12A2,2 0 0,1 10,10A2,2 0 0,1 12,8M16,16H8V15C8,13.67 10.67,13 12,13C13.33,13 16,13.67 16,15V16M13,5H11V1H13V5M16,19H8V18H16V19M12,21H8V20H12V21Z" />
                            </svg>
                            <br>
                            <a>Access to a private channel</a>
                        </div>
                    </div>
                </div>
            </div>
            <object id="breaker2" data="/storage/breaker2.svg" type="image/svg+xml"></object>
            <div class="event1">
                <div class="container">
                    <h1>Challenge 1: The Grinch That Stole Christmas</h1>
                    <br><br>
                    <div class="row">
                        <div class="col-md-2 col-xs-2">
                            <img src="/storage/evilgrinch.png">
                        </div>
                        <div class="col-md-10 col-xs-10">
                            <a>The grinch is once again up to snow good! We've found an odd image located on on his personal website you may find interesting, see if you can unzip the challenge...</a>
                            <br>
                            <a href="/challenge1/grinch.gif" target="_blank">Link to image</a>
                        </div>
                    </div>
                </div>
            </div>
            <object id="breaker3" data="/storage/breaker3.svg" type="image/svg+xml"></object>
            <div class="event2">
                <div class="container">
                    <h1>Challenge 2: The Password Replacement</h1>
                    <br><br>
                    <div class="row">
                        <div class="col-md-4 col-xs-2">
                            <img src="/challenge1/grinch.gif">
                        </div>
                        <div class="col-md-8 col-xs-12">
                            <a>Now that we have a gateway into the network we need access to his machine. Its coding time, baby! The grinch uses a form of authentication that is based on calculating cube roots. See if you can "crack the code".</a>
                            <br><br>
                            <div class="code">
                                <code>
                                <pre><a class="blue">public static</a><a class="yellow"> main</a>() {
    <a class="yellow">Bitmap</a> <a class="l-blue">image</a> = <a class="l-blue">new</a> <a class="green">Bitmap</a>(<a class="orange">"./auth.bmp"</a>);

    <a class="green">//Convert to greyscale</a>

    <a class="green">//Flip</a>

    <a class="l-blue">File</a>.<a class="yellow">write</a>(<a class="l-blue">image</a>, <a class="orange">"/auth_attempt.bmp"</a>);
}</pre>
                                </code>
                            </div>
                            <br>
                            <a>The url is /$challenge_1_key$/code</a>
                        </div>
                    </div>
                </div>
            </div>
            <object id="breaker4" data="/storage/breaker4.svg" type="image/svg+xml"></object>
            <div class="event3">
                <div class="container">
                    <h1>Challenge 3: Why you dont make custom authentication solutions</h1>
                    <br><br>
                    <div class="row">
                        <div class="col-md-3 col-xs-2">
                            <img src="/storage/challenge3.gif">
                        </div>
                        <div class="col-md-9 col-xs-10">
                            <a>From the grinches computer we found the location of his NAS (Network Attached Storage), no saved password, though where would the fun in that be! Try to break in.</a>
                            <br>
                            <a>The url is /$challenge_2_key$/login.html</a>
                        </div>
                    </div>
                </div>
            </div>
            <object id="breaker5" data="/storage/breaker5.svg" type="image/svg+xml"></object>
            <div class="event4">
                <div class="container">
                    <h1>Challenge 4: Blow it sky high</h1>
                    <br><br>
                    <div class="row">
                        <div class="col-md-4 col-xs-2">
                            <img src="/challenge4/grinch.jpg">
                        </div>
                        <div class="col-md-8 col-xs-12">
                            <a>It's go time. You've got access to the <b>Grinch's</b> computer, connect via <b>SSH</b>, find the virus he's written and run the deletion script.<br>For this challenge you need access to SSH terminal, for linux and mac this is built in using the ssh command, for windows you should using something like PuTTY</a>
                            <br><br>
                            <a><b>IP:</b> 138.68.241.128<br><b>Username:</b> UDC<br><b>Password:</b> The key from event 3</a>
                        </div>
                    </div>
                </div>
            </div>
            <object id="breaker6" data="/storage/breaker6.svg" type="image/svg+xml"></object>
            <div class="event5">
                <div class="container">
                    <h1>Challenge 5: The Cautionary Tale</h1>
                    <br><br>
                    <div class="row">
                        <div class="col-md-4 col-xs-2">
                            <img src="/challenge5/image.gif">
                        </div>
                        <div class="col-md-8 col-xs-12">
                            <a>You saved christmas! Now the gatekeeper for your reward is all thats left... Give him a nice message and he may go easier on you ;)</a>
                            <br>
                            <a>Message <b>@UDCSanta#9367</b> on Discord</a>
                        </div>
                    </div>
                </div>
            </div>
            <object id="breaker7" data="/storage/breaker7.svg" type="image/svg+xml"></object>
            <div class="credit">
                <div class="container">
                    <div class="col-12">
                        <p>Made By <a href="https://gitlab.com/darkzek/" target="_blank">DarkZek (Marshall Ashdowne)</a> for UDC (Unity Developer Community) in 2019.</p>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>
