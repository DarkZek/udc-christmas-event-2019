package com.darkzek.christmas2019.stages;

import com.darkzek.christmas2019.StageManager;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public interface StageInterface {

    void OnStartup(StageManager manager);

    void OnShutdown();

    void OnMessage(MessageReceivedEvent event);

    void OnJoinStage(PrivateChannel channel);
}
