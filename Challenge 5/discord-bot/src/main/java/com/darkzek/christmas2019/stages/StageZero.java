package com.darkzek.christmas2019.stages;

import com.darkzek.christmas2019.StageManager;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.ArrayList;
import java.util.List;

/*
    This stage is just ensuring that they completed all the other stages first
 */

public class StageZero implements StageInterface {

    List<String> explainedTousers = new ArrayList<String>();
    StageManager manager;

    @Override
    public void OnStartup(StageManager _manager) {
        manager = _manager;
    }

    @Override
    public void OnMessage(MessageReceivedEvent event) {

        if (!explainedTousers.contains(event.getAuthor().getId())) {
            explainedTousers.add(event.getAuthor().getId());

            event.getPrivateChannel().sendMessage("Security Check: What was the stage 4 key?\nFormat: `Key: YOUR_KEY_GOES_HERE`").queue();
            return;
        }

        if (event.getMessage().getContentDisplay().contains("GRANDMA_GOT_RAN_OVER_BY_A_REINDEER")) {
            //Success
            event.getPrivateChannel().sendMessage("Correct! Moving you to stage one...").queue();
            manager.NextStage(event.getAuthor().getId(), event.getPrivateChannel());
            return;
        } else {
            event.getPrivateChannel().sendMessage("Incorrect key").queue();
        }
    }

    @Override
    public void OnJoinStage(PrivateChannel channel) {

    }

    @Override
    public void OnShutdown() {

    }
}
