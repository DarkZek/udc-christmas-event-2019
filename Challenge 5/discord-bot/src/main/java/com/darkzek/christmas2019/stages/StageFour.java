package com.darkzek.christmas2019.stages;

import com.darkzek.christmas2019.StageManager;
import com.darkzek.christmas2019.live.Entries;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

public class StageFour implements StageInterface {

    Entries entries = new Entries();
    JDA jda;

    @Override
    public void OnStartup(StageManager manager) {
        entries.LoadEntries(jda);
    }

    @Override
    public void OnShutdown() {
        entries.SaveEntries();
    }

    @Override
    public void OnMessage(MessageReceivedEvent event) {
        event.getPrivateChannel().sendMessage("Thanks for participating!").queue();
    }

    @Override
    public void OnJoinStage(PrivateChannel channel) {

    }
}
