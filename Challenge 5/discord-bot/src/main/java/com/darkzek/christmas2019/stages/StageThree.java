package com.darkzek.christmas2019.stages;

import com.darkzek.christmas2019.Settings;
import com.darkzek.christmas2019.StageManager;
import net.dv8tion.jda.api.EmbedBuilder;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.awt.*;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Random;

public class StageThree implements StageInterface {

    HashMap<String, Integer> questionLinks = new HashMap<>();
    int completed = 0;
    StageManager manager;
    String[] questions = new String[] {
            "What are your 2020 goals",
            "Whats your favorite christmas meal",
            "What do you like most in the world",
            "Is Die hard a christmas movie",
            "Whats a link to something of yours",
            "Pineapple on Pizza or nah?",
            "Whats the best copypasta",
            "What meme should win best meme of the decade"
    };

    @Override
    public void OnStartup(StageManager _manager) {
        manager = _manager;
        try {
            completed = Integer.parseInt(Files.readString(Paths.get("completed.txt")).trim());
        } catch (IOException e) {
            completed = 0;
        }
    }

    @Override
    public void OnShutdown() {
        try {
            Files.writeString(Paths.get("completed.txt"), completed + "");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void OnMessage(MessageReceivedEvent event) {
        if (!questionLinks.containsKey(event.getAuthor().getId())) {
            OnJoinStage(event.getPrivateChannel());
            return;
        }
        //Done!
        completed++;
        String message = event.getMessage().getContentStripped();

        if (message.length() > 1000) {
            message = message.substring(0, 1000) + "...";
        }

        EmbedBuilder embed = new EmbedBuilder();
        embed.addField("Another user completed the challenge!", event.getAuthor().getAsTag(), false);
        embed.addField("Their answer to the question: " + questions[questionLinks.get(event.getAuthor().getId())], message, false);
        embed.setColor(Color.GREEN);

        String prefix = "";
        switch (completed) {
            case 1: { prefix = "st"; break; }
            case 2: { prefix = "nd"; break; }
            case 3: { prefix = "rd"; break; }
            default: { prefix = "th"; break; }
        }

        embed.setFooter( "They finished " + completed + prefix);

        event.getJDA().getTextChannelById(Settings.EVENT_CHANNEL).sendMessage(embed.build()).queue();
        event.getPrivateChannel().sendMessage("Congratulations on winning! Thank you so much for taking part ;)").queue();

        event.getJDA().getTextChannelById(Settings.EVENT_CHANNEL).sendMessage("!ChristmasCompleted " + event.getAuthor().getId()).queue();
        event.getJDA().getGuildById(Settings.GUILD_ID).addRoleToMember(event.getAuthor().getId(), event.getJDA().getRolesByName("2019 Christmas Event Winner", true).get(0)).queue();

        questionLinks.remove(event.getAuthor().getId());
        manager.NextStage(event.getAuthor().getId(), event.getPrivateChannel());
    }

    @Override
    public void OnJoinStage(PrivateChannel channel) {
        int questionNumber = new Random().nextInt(questions.length);

        channel.sendMessage("Alright you're done trooper! Just a final question ||this will be posted to UDC Chat so dont be too dumb ;) ||.\n" + questions[questionNumber]).queue();

        questionLinks.put(channel.getUser().getId(), questionNumber);
    }
}
