package com.darkzek.christmas2019;

import com.darkzek.christmas2019.stages.*;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class StageManager extends ListenerAdapter {

    public HashMap<String, Integer> stages = new HashMap();
    public JDA jda;
    StageInterface[] stageClasses = new StageInterface[] { new StageZero(), new StageOne(), new StageTwo(), new StageThree(), new StageFour()};

    @Override
    public void onMessageReceived(MessageReceivedEvent event)
    {
        if (!event.isFromType(ChannelType.PRIVATE) || event.getAuthor().isBot()) {
            return;
        }

        //Get stage
        int stage = GetStage(event.getAuthor().getId());

        //Send message to stage
        stageClasses[stage].OnMessage(event);
    }

    public void LoadStages() {

        //If file doesnt exist dont load
        if (!Files.exists(Path.of("./users.json"))) {
            return;
        }

        try {
            List<String> lines = Files.readAllLines(Path.of("./users.json"));
            stages = new HashMap();

            for(int i = 0; i < lines.size(); i++) {
                String line = lines.get(i);
                String[] parts = line.split(": ");
                if (parts.length != 2) {
                    continue;
                }
                stages.put(parts[0], Integer.parseInt(parts[1]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Load stage classes
        for (StageInterface stage : stageClasses) {
            stage.OnStartup(this);
        }
    }

    public void SaveStages() {
        String fileString = "";

        for (Map.Entry<String, Integer> user : stages.entrySet()) {
            fileString += user.getKey() + ": " + user.getValue() + "\n";
        }

        try {
            Files.writeString(Path.of("./users.json"), fileString);
        } catch (IOException e) {
            e.printStackTrace();
        }

        for (StageInterface stage : stageClasses) {
            stage.OnShutdown();
        }
    }

    public void NextStage(String userId, PrivateChannel messages) {
        int newStage = stages.get(userId) + 1;
        stages.replace(userId, newStage);

        stageClasses[newStage].OnJoinStage(messages);
    }

    private int GetStage(String userId) {

        if (stages.containsKey(userId)) {
            return stages.get(userId);
        }

        //Create new entry
        stages.put(userId, 0);
        return 0;
    }
}
