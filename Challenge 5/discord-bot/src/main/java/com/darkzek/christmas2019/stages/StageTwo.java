package com.darkzek.christmas2019.stages;

import com.darkzek.christmas2019.StageManager;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StageTwo implements StageInterface {

    StageManager manager;

    @Override
    public void OnStartup(StageManager _manager) {
        manager = _manager;
    }

    @Override
    public void OnShutdown() {

    }

    @Override
    public void OnMessage(MessageReceivedEvent event) {
        manager.NextStage(event.getAuthor().getId(), event.getPrivateChannel());
    }

    @Override
    public void OnJoinStage(PrivateChannel channel) {
        manager.NextStage(channel.getUser().getId(), channel);
    }
}
