package com.darkzek.christmas2019.live;

import net.dv8tion.jda.api.JDA;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Random;
import java.util.Set;

public class RunEntries {

    public JDA jda;

    public HashMap<String, Integer> Run(Set<String> entries) {

        int challengeNumber = new Random().nextInt(1000);
        HashMap<String, Integer> scores = new HashMap();

        System.out.println("Running challenge with answer: " + challengeNumber);

        int inputNumber = challengeNumber * challengeNumber * challengeNumber;

        for (String entry : entries) {
            RunResult result = RunEntry(entry, inputNumber);

            if (result.error) {
                System.out.println("There was an error!: " + result.result);

                //Tell user
                jda.getPrivateChannelById(entry).sendMessage("Your UDC entry had a problem. Here's the error log and I used input " + challengeNumber + "\nStacktrace: ```" + result.result + "```").queue();
                continue;
            } else {

                if (result.result.equals(challengeNumber)) {
                    //They got it !
                    System.out.println("Your code returned: " + result.result + " after " + result.msRuntime + "ms");

                    //Points calculation
                    int points = 20 - result.msRuntime;
                    scores.put(entry, points);

                } else {
                    //They didnt get it
                    jda.getPrivateChannelById(entry).sendMessage("Your UDC live event entry had a problem. I provided `" + inputNumber + "` but `" + challengeNumber + "` was expected." ).queue();
                }
                System.out.println("Your code returned: " + result.result + " after " + result.msRuntime + "ms");
            }
        }

        return scores;
    }

    private RunResult RunEntry(String entry, int input) {
        System.out.println("Running code");

        //Write input
        try {
            Files.writeString(Paths.get(Entries.directory + "/" + entry + "/input.txt"), input + "");
        } catch (IOException e) {
            e.printStackTrace();
        }

        Process process = null;
        try {
            process = Runtime.getRuntime().exec("docker run --rm -m 100M --cpuset-cpus=0 --cpus=1 --cpu-shares=256 -v " + Entries.directory + "/" + entry + "/:/app docker_live_dotnet");
        } catch (IOException e) {
            e.printStackTrace();
        }

        BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));

        BufferedReader errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));

        String log = "";
        String s = null;
        try {
            while ((s = reader.readLine()) != null) {
                log += s + "\n";
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        log = log.trim();

        String error = "";
        try {
            while ((s = errorReader.readLine()) != null) {
                error += s;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        //Remove non errors
        error = error.replaceAll("WARNING: Your kernel does not support swap limit capabilities or the cgroup is not mounted. Memory limited without swap.", "").trim();

        //An error was thrown
        if (!error.equals("")) {
            return new RunResult().SetError(true).SetResult(error);
        }

        if (log.contains("Build FAILED.")) {
            return new RunResult().SetError(true).SetResult(log);
        }

        String[] lines = log.split("\n");
        String output = lines[lines.length - 2];
        int msTaken = Integer.parseInt(lines[lines.length - 1]);

        return new RunResult().SetResult(output.trim()).SetMsRuntime(msTaken);
    }
}
