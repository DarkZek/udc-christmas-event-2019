package com.darkzek.christmas2019;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import net.dv8tion.jda.api.entities.GuildChannel;
import net.dv8tion.jda.api.entities.Invite;

import javax.security.auth.login.LoginException;

public class Main {

    public static StageManager stageManager = new StageManager();

    public static void main(String[] args) {
        try {
            JDA jda = new JDABuilder("NTE0OTc5MTYxMTQ0NTU3NjAw.XgWabg.DZB0R1mBZkScYAwssi9TCecloXg").build();

            jda.awaitReady();

            stageManager.jda = jda;
            stageManager.LoadStages();
            jda.addEventListener(stageManager);

            Runtime.getRuntime().addShutdownHook(new Thread(() -> {
                stageManager.SaveStages();
                jda.shutdown();
            }));

        } catch (LoginException | InterruptedException e) {
            System.out.println(e);
            return;
        }
    }
}
