package com.darkzek.christmas2019.stages;

import com.darkzek.christmas2019.StageManager;
import net.dv8tion.jda.api.entities.PrivateChannel;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;

import java.util.HashMap;

public class StageOne implements StageInterface {

    HashMap<String, Integer> progress = new HashMap<String, Integer>();
    StageManager manager;

    String[] questions = new String[] {
            "What does `5 + 5 * 2` resolve to in C#?",
            "Which of these games was made in Unity?\nA) Bioshock Infinite\nB) Kerbal Space Program\nC) Apex Legends\nD) Life is Strange 2",
            "How many days of christmas does the `On the nth day of Christmas, my true love brang to me...` song count up to?",
            "Rounded to the nearest 100 how many users does UDC have?"};

    String[][] answers = new String[][] {
            new String[] {"15"},
            new String[] {"B", "Kerbal Space Program"},
            new String[] {"12", "Twelve"},
            new String[] {"5600"}};

    @Override
    public void OnStartup(StageManager _manager) {
        manager = _manager;
    }

    @Override
    public void OnMessage(MessageReceivedEvent event) {

        if (!progress.containsKey(event.getAuthor().getId())) {
            OnJoinStage(event.getPrivateChannel());
            return;
        }

        //Read answer
        String message = event.getMessage().getContentDisplay();
        int questionNumber = progress.get(event.getAuthor().getId());

        for (String answer : answers[questionNumber]) {
            if (message.trim().equalsIgnoreCase(answer)) {
                //Got it!
                NextQuestion(event, questionNumber);
                return;
            }
        }

        event.getPrivateChannel().sendMessage("Err, try again.").queue();
    }

    public void NextQuestion(MessageReceivedEvent event, int questionNumber) {
        questionNumber++;

        if (questionNumber == 4) {
            //Done!
            event.getPrivateChannel().sendMessage("Trivia time's over! Moving to stage two...").queue();
            progress.remove(event.getAuthor().getId());
            manager.NextStage(event.getAuthor().getId(), event.getPrivateChannel());
            return;
        }

        progress.replace(event.getAuthor().getId(), questionNumber);

        event.getPrivateChannel().sendMessage("**CORRECT!**\nQ " + questionNumber + ": " + questions[questionNumber] ).queue();
    }


    @Override
    public void OnJoinStage(PrivateChannel channel) {
        channel.sendMessage("Now for some Trivia!\n" + questions[0]).queue();
        progress.put(channel.getUser().getId(), 0);
    }

    @Override
    public void OnShutdown() {

    }
}
