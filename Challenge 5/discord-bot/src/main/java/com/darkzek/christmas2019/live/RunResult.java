package com.darkzek.christmas2019.live;

public class RunResult {
    public int msRuntime;
    public String userId;
    public boolean correct;
    public String result;
    public boolean error = false;

    public RunResult SetResult(String _result) {
        this.result = _result;
        return this;
    }

    public RunResult SetMsRuntime(int _msRuntime) {
        this.msRuntime = _msRuntime;
        return this;
    }

    public RunResult SetUserId(String _userId) {
        this.userId = _userId;
        return this;
    }

    public RunResult SetCorrect(Boolean _correct) {
        this.correct = _correct;
        return this;
    }

    public RunResult SetError(Boolean _error) {
        this.error = _error;
        return this;
    }
}
