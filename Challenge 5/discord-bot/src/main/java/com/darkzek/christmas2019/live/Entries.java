package com.darkzek.christmas2019.live;

import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.entities.User;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Entries {

    HashMap<String, Integer> users = new HashMap();
    public static String directory = "/home/darkzek/projects/udc-christmas-event-2019/docker/live_dotnet/live";
    RunEntries runner = new RunEntries();

    public void SaveEntries() {
        String fileString = "";

        for (Map.Entry<String, Integer> user : users.entrySet()) {
            fileString += user.getKey() + ": " + user.getValue() + "\n";
        }

        try {
            Files.writeString(Path.of("./entries.json"), fileString);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void LoadEntries(JDA _jda) {
        runner.jda = _jda;

        //If file doesnt exist dont load
        if (!Files.exists(Path.of("./entries.json"))) {
            return;
        }

        try {
            List<String> lines = Files.readAllLines(Path.of("./entries.json"));
            users = new HashMap();

            for(int i = 0; i < lines.size(); i++) {
                String line = lines.get(i);
                String[] parts = line.split(": ");
                if (parts.length != 2) {
                    continue;
                }
                users.put(parts[0], Integer.parseInt(parts[1]));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        //RunRound();
    }

    public void RunRound() {
        HashMap<String, Integer> scores = runner.Run(users.keySet());

        //Sort
        Map<String, Integer> sortedMap =
                scores.entrySet().stream()
                        .sorted(Map.Entry.comparingByValue())
                        .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,
                                (e1, e2) -> e1, LinkedHashMap::new));

        for (Map.Entry<String, Integer> user : sortedMap.entrySet()) {
            
        }
    }

    public void EnterUser(User user, String code) {
        users.put(user.getId(), 0);

        try {
            if (!Files.exists(Paths.get(directory + "/example/"))) {
                Runtime.getRuntime().exec("cp -r " + directory + "/example " + directory + "/" + user.getId());
            }
            String template = Files.readString(Path.of(directory + "/example/Program.cs"));
            code = template.replaceAll("%CODE%", code);

            Files.writeString(Path.of(directory + "/" + user.getId() + "/Program.cs"), code);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
