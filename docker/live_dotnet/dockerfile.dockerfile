FROM mcr.microsoft.com/dotnet/core/sdk:3.1

WORKDIR /app

RUN useradd UDC
RUN mkdir /home/UDC/
RUN mkdir /home/UDC/.dotnet/
RUN touch /home/UDC/.dotnet/"$(dotnet --version)".dotnetFirstUseSentinel
RUN chmod 777 -R /home/UDC/
COPY sysctl.conf /etc/sysctl.conf
CMD dotnet build && su UDC -c "bash -c 'dotnet ./bin/Debug/netcoreapp3.1/app.dll $(cat /app/input.txt)'"