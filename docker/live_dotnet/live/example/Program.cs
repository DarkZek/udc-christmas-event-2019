using System;
using System.IO;

namespace Program {
    class Program {
        static void Main(string[] args) {

            long input = long.Parse(File.ReadAllLines("/app/input.txt")[0]);

            var watch = System.Diagnostics.Stopwatch.StartNew();
            
            Compute(input);

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;

            Console.WriteLine(elapsedMs);
        }

        %CODE%
    }
}