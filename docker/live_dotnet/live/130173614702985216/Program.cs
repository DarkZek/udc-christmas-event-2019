using System;
using System.IO;

namespace Program {
    class Program {
        static void Main(string[] args) {

            Console.WriteLine(File.ReadAllLines("/app/input.txt")[0]);

            var watch = System.Diagnostics.Stopwatch.StartNew();
            
            Compute(long.Parse(args[0]));

            watch.Stop();
            var elapsedMs = watch.ElapsedMilliseconds;

            Console.WriteLine(elapsedMs);
        }

        static void Compute(long input) {
    Console.WriteLine(Math.Floor(3.14f));
}
    }
}