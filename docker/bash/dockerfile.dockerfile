from rastasheep/ubuntu-sshd:16.04

RUN useradd UDC
RUN echo "UDC:merrychristmaseveryone" | /usr/sbin/chpasswd
RUN echo "UDC hard nproc 100" >> /etc/security/limits.conf

#Set default shell for UDC user to be /bin/sh
RUN sed '$ s/$/\/bin\/bash/' /etc/passwd > /etc/passwd.new && mv /etc/passwd.new /etc/passwd

#Set root password
RUN echo "root:YHHtaZpLpjGzbDngKcdVuPxQM3zzhSc6Lp" | chpasswd

#Create secret
RUN mkdir /lib/ru1n_ch41stma3/
RUN echo "#!/bin/bash\necho GRANDMA_GOT_RAN_OVER_BY_A_REINDEER" > /lib/ru1n_ch41stma3/execute.sh
RUN echo "[Script] Generating virus at /lib/ru1n_ch41stma3/execute.sh\n[Script] Success! Run the script when ready..." > /var/log/virus_generator.log

RUN chmod 500 /usr/bin/passwd
RUN apt update && apt install nano
RUN chsh -s /bin/bash UDC
RUN chmod 555 -R /home/
RUN usermod -d / UDC

#Make it secure
RUN chmod 755 -R /var/
RUN chmod 755 -R /tmp/
RUN chmod +x /lib/ru1n_ch41stma3/execute.sh