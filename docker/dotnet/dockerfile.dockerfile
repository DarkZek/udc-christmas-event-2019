FROM mcr.microsoft.com/dotnet/core/sdk:3.1

WORKDIR /app

RUN useradd UDC
RUN mkdir /home/UDC/
RUN mkdir /home/UDC/.dotnet/
RUN touch /home/UDC/.dotnet/"$(dotnet --version)".dotnetFirstUseSentinel
RUN chmod 777 -R /home/UDC/
COPY sysctl.conf /etc/sysctl.conf
CMD su UDC -c "timeout 5 dotnet run Program.cs"