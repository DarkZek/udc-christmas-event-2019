<?php
if (is_null($_POST["code"])) {
    die("No code submitted");
}
if (strpos($_POST["code"], "Math") !== false) {
    die("Please dont use the C# Math lib, if you arent then you cannot have the string Math in your code.");
}

$dir = "/home/darkzek/projects/udc-christmas-event-2019/docker/dotnet/codes";
$newDir = "/home/darkzek/projects/udc-christmas-event-2019/docker/dotnet/codes/";
$id = rand(0, 9999999999);
$number = rand(0, 500);
$input = $number * $number * $number;
shell_exec("cp -R $dir/example $newDir/$id");
shell_exec("echo $input >> $newDir/$id/input.txt");
shell_exec("chmod 777 -R $newDir/$id/");

$boilerplate = file_get_contents("$newDir/$id/Program.cs");
$code = str_replace("%CODE%", $_POST["code"], $boilerplate);
file_put_contents("$newDir/$id/Program.cs", $code);

$result = shell_exec("docker run --rm -m 200M --cpuset-cpus='0' --cpus=1 --cpu-shares=256 -v $newDir/$id/:/app docker_dotnet");

if (trim($result) == $number) {
    echo("You got it right! The key is `FELIZ_NAVIDAD`");
} else {
    echo("You didnt get it right. $number was expected but you returned $result");
}

//Key is FELIZ_NAVIDAD