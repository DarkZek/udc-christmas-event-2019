<html>
    <head>
        <title>Grinch IDE</title>
        <style>
            body {
                overflow: hidden;
                font-family: Noto Sans;
                margin: 0px;
            }
            #editor {
                height: 60vh;
            }
            .header {
                width: 100vw;
                display: block;
                color: white;
                background-color: #424242;
                padding: 15px;
                font-weight: bold;
                max-height: 53px;
            }
            .btn {
                background-color: #339116;
                border-radius: 5px;
                padding: 8px;
                float: right;
                min-width: 100px;
                margin-top: -8px;
                margin-right: 22px;
                text-align: center;
                cursor: pointer;
                user-select: none;
            }
            .ide {
                height: 60vh;
                width: 100vw;
                background-color: #323232;
            }
            .result {
                height: calc(40vh - 53px);
                max-height: calc(40vh - 53px);
                overflow-x: hidden;
                overflow-y: auto;
                width: 100vw;
                background-color: #444444;
                border-top: 1px #828282 solid;
                color: white;
            }

            #result {
                width: 100vw;
                overflow-x: auto;
                display: block;
                white-space: pre-line;
            }

            .result .title {
                width: 100vw;
                height: 30px;
                border-bottom: 1px #828282 solid;
                margin-top: 3px;
                padding-left: 20px;
                color: white;
            }

            .result-padding {
                padding: 15px;
            }
        </style>
        <script src="/js/app.js"></script>
        <script src="./script.js"></script>
        <input type="hidden" name="_token" value="<?php echo csrf_token(); ?>">
    </head>
    <body>
        <div class="header">
            <a>Grinch IDE</a>
            <a class="btn" onclick="window.Run()">RUN</a>
        </div>
        <div class="ide">
            <div id="editor">static void Main(string[] args) {
    int input = Int32.Parse(System.IO.File.ReadAllText("./input.txt"));
    
    //Calculate cube root

    
    Console.WriteLine(input);
}</div>
                
            <script src="./src-min-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
            <script>
                var editor = ace.edit("editor");
                editor.setTheme("ace/theme/monokai");
                editor.session.setMode("ace/mode/csharp");
                window.editor = editor;
            </script>
        </div>
        <div class="result">
            <div class="title">
                <a>Result</a>
            </div>
            <div class="result-padding">
                <a id="result"></a>
            </div>
        </div>
    </body>
</html>