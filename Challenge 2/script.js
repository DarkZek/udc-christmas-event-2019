window.Run = function() {

    $("#result")[0].innerHTML = "Compiling...";

    var code = window.editor.getValue();

    var resp = $.post("./run", { code,  _token: $("*[name=_token").val() });

    resp.done(function(status, text, response) {
        $("#result")[0].innerHTML = response.responseText;
    })
    .fail(function() { $("#result")[0].innerHTML = "You're doing that too much." });
};